import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PortfolioService } from '../portfolio.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  private profile: any;
  private subscription: Subscription;

  constructor(private portfolioService: PortfolioService) { }

  ngOnInit(){
    this.subscription = this.portfolioService.getPortfolio().subscribe(response=>{
      this.profile = response;
    });
    this.portfolioService.initializeBody();
  }

}
