import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressComponent } from './address/address.component';
import { AppComponent } from './app.component';
import { BirthdayComponent } from './birthday/birthday.component';
import { EmailComponent } from './email/email.component';
import { NameComponent } from './name/name.component';
import { PasswordComponent } from './password/password.component';
import { PhoneComponent } from './phone/phone.component';


const routes: Routes = [{
  path: '', component: NameComponent
},{
  path: 'address', component: AddressComponent
},{
  path: 'birthday', component: BirthdayComponent
},{
  path: 'email', component: EmailComponent
},{
  path: 'name', component: NameComponent
},{
  path: 'password', component: PasswordComponent
},{
  path: 'phone', component: PhoneComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
