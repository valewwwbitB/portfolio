import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PortfolioService } from './portfolio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit{
  private profile: any;
  private subscription: Subscription;

  constructor(private portfolioService: PortfolioService, private router: Router){
  }

  ngOnInit(){
    this.portfolioService.getData();
    this.subscription = this.portfolioService.getPortfolio().subscribe(response=>{
      this.profile = response;
    });
  }

  ngAfterViewInit(){
    // this.portfolioService.getData();
    // this.subscription = this.portfolioService.getPortfolio().subscribe(response=>{
    //   this.profile = response;
    // });
  }

  redirect(path: string){
    this.portfolioService.setPortfolio(this.profile);
    this.router.navigate([path]);
  }

  getProfilePicSrc():string {
    return this.profile ? this.profile.picture.large : '';
  }
}
