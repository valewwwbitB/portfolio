import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NameComponent } from './name/name.component';
import { AddressComponent } from './address/address.component';
import { BirthdayComponent } from './birthday/birthday.component';
import { EmailComponent } from './email/email.component';
import { PasswordComponent } from './password/password.component';
import { PhoneComponent } from './phone/phone.component';
import { HttpModule } from '@angular/http';
import { PortfolioService } from './portfolio.service';

@NgModule({
  declarations: [
    AppComponent,
    NameComponent,
    AddressComponent,
    BirthdayComponent,
    EmailComponent,
    PasswordComponent,
    PhoneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [PortfolioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
