import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PortfolioService } from '../portfolio.service';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.css']
})
export class BirthdayComponent implements OnInit {

  private profile: any;
  private subscription: Subscription;

  constructor(private portfolioService: PortfolioService) { }

  ngOnInit(){
    this.subscription = this.portfolioService.getPortfolio().subscribe(response=>{
      this.profile = response;
    });
    this.portfolioService.initializeBody();
  }

}
