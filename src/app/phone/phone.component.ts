import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PortfolioService } from '../portfolio.service';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.css']
})
export class PhoneComponent implements OnInit {

  private profile: any;
  private subscription: Subscription;
  
  constructor(private portfolioService: PortfolioService) { }

  ngOnInit(){
    this.subscription = this.portfolioService.getPortfolio().subscribe(response=>{
      this.profile = response;
    });
    this.portfolioService.initializeBody();
  }

}
