import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService{

  constructor(private http: Http) {
  }
  public profile : any;
  public _portfolio: Subject<any> = new Subject();
  public portfolio: Observable<any> = this._portfolio.asObservable();

  getData(){
    this.http.get('https://randomuser.me/api').subscribe(response=>{
      this.profile = response.json().results[0];
      this.setPortfolio(this.profile);
    });
  }

  setPortfolio(value:any){
    this._portfolio.next(value);
    this.portfolio = this._portfolio.asObservable();
  }

  getPortfolio(): Observable<any>{    
    return this._portfolio.asObservable();
  }

  initializeBody() {
    this._portfolio.next(this.profile);
    this.portfolio = this._portfolio.asObservable();
  }

}
